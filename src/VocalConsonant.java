import java.util.Scanner;

public class VocalConsonant {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("|| PROGRAM HURUF VOKAL DAN KONSONAN ||");
        System.out.println("============================");
        System.out.println("Masukkan Kata atau Kalimat : ");
        String kata = scanner.nextLine();
        System.out.println("Huruf vokalnya : ");
        vowel(kata);
        System.out.println();
        System.out.println("Huruf Konsonannya : ");
        consonant(kata);
        System.out.println();
        System.out.println("Jumlah Karakter : " + kata.length());
    }

    public static void vowel(String kata) {


        for (int i = 0; i < kata.length(); i++) {
            if (kata.split("")[i].equals("a") || kata.split("")[i].equals("i") || kata.split("")[i].equals("u") || kata.split("")[i].equals("e") || kata.split("")[i].equals("o") || kata.split("")[i].equals("A") || kata.split("")[i].equals("I") || kata.split("")[i].equals("U") || kata.split("")[i].equals("E") || kata.split("")[i].equals("O")) {
                System.out.print(kata.split("")[i] + " ");
            }
        }
    }

    public static void consonant(String kata) {
        for (int i = 0; i < kata.length(); i++) {
            if (kata.split("")[i].equals("b") || kata.split("")[i].equals("c") || kata.split("")[i].equals("d") || kata.split("")[i].equals("f") || kata.split("")[i].equals("g") || kata.split("")[i].equals("h") || kata.split("")[i].equals("j") || kata.split("")[i].equals("k") || kata.split("")[i].equals("l") || kata.split("")[i].equals("m") || kata.split("")[i].equals("n") || kata.split("")[i].equals("p") || kata.split("")[i].equals("q") || kata.split("")[i].equals("r") || kata.split("")[i].equals("s") || kata.split("")[i].equals("t") || kata.split("")[i].equals("v") || kata.split("")[i].equals("w") || kata.split("")[i].equals("x") || kata.split("")[i].equals("y") || kata.split("")[i].equals("z") || kata.split("")[i].equals("B") || kata.split("")[i].equals("C") || kata.split("")[i].equals("D") || kata.split("")[i].equals("F") || kata.split("")[i].equals("G") || kata.split("")[i].equals("H") || kata.split("")[i].equals("J") || kata.split("")[i].equals("K") || kata.split("")[i].equals("L") || kata.split("")[i].equals("M") || kata.split("")[i].equals("N") || kata.split("")[i].equals("P") || kata.split("")[i].equals("Q") || kata.split("")[i].equals("R") || kata.split("")[i].equals("S") || kata.split("")[i].equals("T") || kata.split("")[i].equals("V") || kata.split("")[i].equals("W") || kata.split("")[i].equals("X") || kata.split("")[i].equals("Y") || kata.split("")[i].equals("Z")) {
                System.out.print(kata.split("")[i] + " ");
            }
        }
    }
}


