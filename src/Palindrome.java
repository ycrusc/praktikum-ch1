import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("|| PROGRAM KATA PALINDROME ||");
        System.out.println("============================");
        System.out.println("Masukkan Kata : " );
        String s = scanner.nextLine();
        System.out.println("Hasil : " + s + " adalah " + isPalindrome(s));
    }

    public static String isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return "palindrome";
        }
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                return "bukan palindrome";
            }
            i++;
            j--;
        }
        return " palindrome";
    }
}
